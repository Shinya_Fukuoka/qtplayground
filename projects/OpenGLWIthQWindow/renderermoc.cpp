#include "renderermoc.h"
#include <iostream>

#define ASSERT(x) if (!(x)) __debugbreak();

#define GLCall(x) GLClearError();\
	x;\
	ASSERT(GLLogCall());

static void GLClearError()
{
	while (glGetError() != GL_NO_ERROR);
}

static bool GLLogCall()
{
	while (GLenum error = glGetError())
	{
		std::cout << "|OpenGL Error| (" << error << ")" << std::endl;
		// std::cout  << error << std::endl;
		return false;
	}
	return true;
}


RendererMoc::RendererMoc()
{

}
RendererMoc::~RendererMoc()
{


}


void RendererMoc::Initialize(HWND winId)
{
	glewExperimental = GL_TRUE;
	auto result = glewInit();
	std::cout << result << std::endl;

}

void RendererMoc::MakeCurrent(HWND winId)
{
	std::cout << "WinId : " << winId << "\n";
	auto hdc = GetDC(winId);
	m_hdcDict[winId] = hdc;
	m_currentHWND = winId;

	PIXELFORMATDESCRIPTOR pd = {0};
	pd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pd.nVersion = 1;
	pd.iPixelType = PFD_TYPE_RGBA;
	pd.cColorBits = 32;
	pd.cDepthBits = 24;
	pd.cStencilBits = 8;

	auto pixelFormat = ChoosePixelFormat(hdc, &pd) ;
	SetPixelFormat(hdc, pixelFormat, &pd);
	m_glContextDict[hdc] = wglCreateContext(hdc);

	std::cout << "MakeCurrent" << std::endl;
	// GLCall(wglMakeCurrent(m_hdcDict[winId], m_glContextDict[m_hdcDict[winId]]));
	if (!wglMakeCurrent(m_hdcDict[winId], m_glContextDict[m_hdcDict[winId]]))
	{
		std::cout << "Context creation failed." << "\n";
	}else
	{
		std::cout << "Context creation succeeded." << "\n";
	}
	std::cout << "MakeCurrent Finished." << std::endl;
	// std::cout << result << "\n";
}

void RendererMoc::Render()
{
	auto hdc = GetDC(m_currentHWND);

	// GLuint vao;
	// glGenVertexArrays(1, &vao);
	// glBindVertexArray(vao);

	glClearColor(0.45f, 0.45f, 0.45f, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	const GLfloat vertices[] = {
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f
	};

	GLuint vbo;
	GLCall(glGenBuffers(1, &vbo));
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, vbo));
	GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW));
	
	glEnableVertexAttribArray(0);

	// glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glVertexAttribPointer(
	0,
	3,
	GL_FLOAT,
	GL_FALSE,
	0,
	(void*)0
	);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glDisableVertexAttribArray(0);
	SwapBuffers(hdc);

}