

#include <iostream>


#include <string>
#include <fstream>

#include <stdlib.h>

#include <QWindow>
#include <QApplication>

#include "renderermoc.h"

// #include <QApplication>
// #include <QMainWindow>
// #include <QSurfaceFormat>
// #include <QCommandLineParser>
// #include <QCommandLineOption>
// #include "mainwindow.h"

void printShader(const char* filepath)
{
	std::ifstream ifs(filepath);
	std::string line;

	if (!ifs)
	{
		std::cout << std::string(filepath) << " couldn't be opened. " << std::endl;
	}

	int lineNum = 1;
	while (std::getline(ifs, line))
	{
		std::cout << lineNum << ": " << line << std::endl;
		lineNum += 1;
	}
	std::cout << "\n" << std::endl;

}



int main( int argc, char ** argv )
{
	QApplication app(argc, argv);


	QWindow window;
	window.setHeight(300);
	window.setWidth(450);
	// std::cout << std::to_string(window.winId()) << "\n";
	// std::cout << window.winId() << "\n";Pz
	window.show();
	RendererMoc renderer;

	auto hwnd  = (HWND)window.winId();
	renderer.MakeCurrent(hwnd);
	renderer.Initialize(hwnd);
	renderer.Render();

	printShader("src/shaders/vertex.glsl");
	printShader("src/shaders/fragment.glsl");
	// glm::vec3 axis_z(0, 0, 1);

	// if (!glewInit())
	// {
	// 	std::cout << "glewInit() failed.\n";
	// }
	// else
	// {
	// 	std::cout << "glewInit() scceeded.\n";
	// }

	// glm::vec2 a = glm::vec2(3, 4);

	// std::cout
	// 	<< "a:" << a.x << " " << a.y << "\n"
	// 	<< "length(a)" << glm::length(a)
	// 	;



	return app.exec();
	// std::cin.get();
    //Q_INIT_RESOURCE(texture);
    // QApplication a( argc, argv );

    // QCoreApplication::setApplicationName("Qt QOpenGLWidget Example");
    // QCoreApplication::setOrganizationName("QtProject");
    // QCoreApplication::setApplicationVersion(QT_VERSION_STR);
    // QCommandLineParser parser;
    // parser.setApplicationDescription(QCoreApplication::applicationName());
    // parser.addHelpOption();
    // parser.addVersionOption();
    // QCommandLineOption multipleSampleOption("multisample", "Multisampling");
    // parser.addOption(multipleSampleOption);
    // QCommandLineOption srgbOption("srgb", "Use sRGB Color Space");
    // parser.addOption(srgbOption);
    // parser.process(a);

    // QSurfaceFormat format;
    // format.setDepthBufferSize(24);
    // format.setStencilBufferSize(8);
    // if (parser.isSet(srgbOption))
    //     format.setColorSpace(QSurfaceFormat::sRGBColorSpace);
    // if (parser.isSet(multipleSampleOption))
    //     format.setSamples(4);
    // QSurfaceFormat::setDefaultFormat(format);

    // MainWindow mw;
    // mw.showMaximized();
    // return a.exec();
}
