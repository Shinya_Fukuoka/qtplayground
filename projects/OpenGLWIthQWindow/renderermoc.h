#pragma once
#include <Windows.h>

#define GLEW_STATIC


#include <GL/glew.h>
#ifdef WIN32
#pragma comment(lib, "glew32.lib")
#endif

#include <GL/GL.h>

#ifdef WIN32
#pragma comment(lib, "opengl32.lib")
#endif


#include <unordered_map>

class RendererMoc
{
public:
	RendererMoc();
	~RendererMoc();
	void Initialize(HWND winId);
	void MakeCurrent(HWND winId);
	void Render();
private:
	std::unordered_map<HWND, HDC> m_hdcDict;
	std::unordered_map<HDC, HGLRC> m_glContextDict;
	HWND m_currentHWND;

};