#version 330 core

uniform vec4 g_color;

// Ouput data
out vec4 color;

void main()
{
	color = g_color;
}