#version 330 core

layout(location = 0) in vec4 g_position;

void main()
{
    gl_Position = g_position;
}

